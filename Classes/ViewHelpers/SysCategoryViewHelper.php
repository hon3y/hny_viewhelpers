<?php
namespace HIVE\HiveViewhelpers\ViewHelpers;


/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner - hive GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;


class SysCategoryViewHelper extends AbstractViewHelper {

    const DATABASE_SYSCATEGORY = 'sys_category';
    const DATABASE_SYSCATEGORY_EVENT = 'sys_category_record_mm';

    /**
     * render($iUid)
     * @param int $iUid
     * @return mixed $oResult
     */
    public function render($iUid = NULL) {

        if ($iUid) {

            // get data
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::DATABASE_SYSCATEGORY);
            $queryBuilder
                ->select(
                    self::DATABASE_SYSCATEGORY . '.uid',
                    self::DATABASE_SYSCATEGORY . '.title'
                )

                ->from(
                    self::DATABASE_SYSCATEGORY
                )

                ->leftJoin(
                    self::DATABASE_SYSCATEGORY,
                    self::DATABASE_SYSCATEGORY_EVENT,
                    self::DATABASE_SYSCATEGORY_EVENT,
                    $queryBuilder->expr()->eq(
                        self::DATABASE_SYSCATEGORY . '.uid',
                        $queryBuilder->quoteIdentifier(
                            self::DATABASE_SYSCATEGORY_EVENT . '.uid_local'
                        )
                    )
                )

                ->where(
                    $queryBuilder->expr()->eq(
                        self::DATABASE_SYSCATEGORY_EVENT . '.tablenames',
                        $queryBuilder->createNamedParameter(
                            'tx_hiveextevent_domain_model_event'
                        )
                    ),
                    $queryBuilder->expr()->eq(
                        self::DATABASE_SYSCATEGORY_EVENT . '.uid_foreign',
                        $queryBuilder->createNamedParameter(
                            $iUid
                        )
                    )
                );

            $oResult = $queryBuilder->execute()->fetchAll();
            return $oResult;

        }

    }

}