<?php
namespace HIVE\HiveViewhelpers\ViewHelpers\Media;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Björn Fromme <fromme@dreipunktnull.com>, dreipunktnull
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */
use FluidTYPO3\Vhs\ViewHelpers\Media\Image\AbstractImageViewHelper;

/**
 * Renders an image tag for the given resource including all valid
 * HTML5 attributes. Derivates of the original image are rendered
 * if the provided (optional) dimensions differ.
 *
 * @author Björn Fromme <fromme@dreipunktnull.com>, dreipunktnull
 * @package Vhs
 * @subpackage ViewHelpers\Media
 */
class ImageMultiplyViewHelper extends AbstractImageViewHelper {

	/**
	 * name of the tag to be created by this view helper
	 *
	 * @var string
	 * @api
	 */
	protected $tagName = 'img';



	/**
	 * Initialize arguments.
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerTagAttribute('usemap', 'string', 'A hash-name reference to a map element with which to associate the image.', FALSE);
		$this->registerTagAttribute('ismap', 'string', 'Specifies that its img element provides access to a server-side image map.', FALSE, '');
		$this->registerTagAttribute('alt', 'string', 'Equivalent content for those who cannot process images or who have image loading disabled.', TRUE);

        $this->registerTagAttribute('r', 'integer', 'R.', FALSE);
        $this->registerTagAttribute('g', 'integer', 'G.', FALSE);
        $this->registerTagAttribute('b', 'integer', 'B.', FALSE);

	}

	/**
	 * Render method
	 *
	 * @return string
	 */
	public function render() {
		$this->preprocessImage();
		$src = $this->preprocessSourceUri($this->mediaSource); //var_dump($src);

        // Hier werden nun die Settings ermittelt.
        // Der erste Parameter von getConfiguration muss 'Settings' lauten.
        // Der zweite Parameter beinhaltet den Namen der Extension.
        // Der dritte Parameter beinhaltet den Namen des Plugins.
        //var_dump($this->configurationManager->getConfiguration('Settings','T2content','tx_t2content'));die;

        $aSettings = $this->configurationManager->getConfiguration('Settings','HiveViewhelpers','tx_hive_viewhelpers');
        //$aSettings = $this->templateVariableContainer->get('settings');
        //var_dump($aSettings); die;

        $filter_r = trim($this->tag->getAttribute('r')) != "" ? trim($this->tag->getAttribute('r')) : $aSettings['production']['falImageSimple']['iR']; // 0
        $filter_g = trim($this->tag->getAttribute('g')) != "" ? trim($this->tag->getAttribute('g')) : $aSettings['production']['falImageSimple']['iG']; //103;
        $filter_b = trim($this->tag->getAttribute('b')) != "" ? trim($this->tag->getAttribute('b')) : $aSettings['production']['falImageSimple']['iB']; //187;
        $suffixe = "_multiply_" . $filter_r . '-' . $filter_g . '-' . $filter_b;
        $path=$src;
        $new_path=$src;

        $sFileExtension = '';

        //$new_path = str_replace('fileadmin/', 'fileadmin/_processed_/', $src);

        $mimetype = '';
        if(function_exists('mime_content_type')){
            $mimetype = mime_content_type($src);
            //return $mimetype;

        } elseif(function_exists('finfo_open')){
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $src);
            finfo_close($finfo);
            //return $mimetype;
        }


        if(is_file($path) && ($filter_r != 255 || $filter_g != 255 || $filter_b != 255)) {

            $bProcess = true;

            switch ($mimetype) {
                case 'image/jpeg':
                    $image=@imagecreatefromjpeg($path);
                    break;
                case 'image/png':
                    $image=@imagecreatefrompng($path);
                    break;
                default:
                    $bProcess = false;
            }

            $new_path= 'fileadmin/_processed_/' . str_replace('.', $suffixe . '.', basename($path));
            if(file_exists($new_path)) {
                $bProcess = false;
            }

            if ($bProcess) {

                //

                // invert inteded color
//                $filter_r_opp = 255 - $filter_r; // = 0
//                $filter_g_opp = 255 - $filter_g; // = 255
//                $filter_b_opp = 255 - $filter_b; // = 255
//                $filter_a_opp = 127 - (127 / 100 * 80);
//
//                /* FAST METHOD */
//                imagefilter($image, IMG_FILTER_NEGATE);
//                imagefilter($image, IMG_FILTER_COLORIZE, $filter_r_opp, $filter_g_opp, $filter_b_opp, $filter_a_opp);
//                imagefilter($image, IMG_FILTER_NEGATE);

                //

                $imagex = imagesx($image);
                $imagey = imagesy($image);
                for ($x = 0; $x <$imagex; ++$x) {
                    for ($y = 0; $y <$imagey; ++$y) {
                        $rgb = imagecolorat($image, $x, $y);
                        $TabColors=imagecolorsforindex ( $image , $rgb );
                        $color_r=floor($TabColors['red']*$filter_r/255);
                        $color_g=floor($TabColors['green']*$filter_g/255);
                        $color_b=floor($TabColors['blue']*$filter_b/255);
                        $newcol = imagecolorallocate($image, $color_r,$color_g,$color_b);
                        imagesetpixel($image, $x, $y, $newcol);
                    }
                }

                switch ($mimetype) {
                    case 'image/png':
                        imagepng($image,$new_path);
                        break;
                    default:
                        imagejpeg($image,$new_path);
                }

                // Release memory
                imagedestroy($image);

            }
        }

        $this->tag->addAttribute('src', $new_path);
		$this->tag->addAttribute('width', $this->imageInfo[0]);
		$this->tag->addAttribute('height', $this->imageInfo[1]);
		if ('' === $this->arguments['title']) {
			$this->tag->addAttribute('title', $this->arguments['alt']);
		}
		return $this->tag->render();
	}

}
