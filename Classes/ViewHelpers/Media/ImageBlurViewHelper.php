<?php
namespace HIVE\HiveViewhelpers\ViewHelpers\Media;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Björn Fromme <fromme@dreipunktnull.com>, dreipunktnull
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */
use FluidTYPO3\Vhs\ViewHelpers\Media\Image\AbstractImageViewHelper;

/**
 * Renders an image tag for the given resource including all valid
 * HTML5 attributes. Derivates of the original image are rendered
 * if the provided (optional) dimensions differ.
 *
 * @author Björn Fromme <fromme@dreipunktnull.com>, dreipunktnull
 * @package Vhs
 * @subpackage ViewHelpers\Media
 */
class ImageBlurViewHelper extends AbstractImageViewHelper {

	/**
	 * name of the tag to be created by this view helper
	 *
	 * @var string
	 * @api
	 */
	protected $tagName = 'img';



	/**
	 * Initialize arguments.
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerTagAttribute('usemap', 'string', 'A hash-name reference to a map element with which to associate the image.', FALSE);
		$this->registerTagAttribute('ismap', 'string', 'Specifies that its img element provides access to a server-side image map.', FALSE, '');
		$this->registerTagAttribute('alt', 'string', 'Equivalent content for those who cannot process images or who have image loading disabled.', TRUE);

        $this->registerTagAttribute('radius', 'integer', 'BlurRadius.', FALSE);

	}

	/**
	 * Render method
	 *
	 * @return string
	 */
	public function render() {
		$this->preprocessImage();
		$src = $this->preprocessSourceUri($this->mediaSource); //var_dump($src);

        // Hier werden nun die Settings ermittelt.
        // Der erste Parameter von getConfiguration muss 'Settings' lauten.
        // Der zweite Parameter beinhaltet den Namen der Extension.
        // Der dritte Parameter beinhaltet den Namen des Plugins.
        //var_dump($this->configurationManager->getConfiguration('Settings','T2content','tx_t2content'));die;

        $aSettings = $this->configurationManager->getConfiguration('Settings','HiveViewhelpers','tx_hive_viewhelpers');
        //$aSettings = $this->templateVariableContainer->get('settings');
        //var_dump($aSettings); die;

        $filter_radius = trim($this->tag->getAttribute('radius')) != "" ? trim($this->tag->getAttribute('radius')) : $aSettings['production']['falImageSimple']['iRadius']; // 0

        $suffixe = "_blur_" . $filter_radius;
        $path=$src;
        $new_path=$src;

        $sFileExtension = '';

        //$new_path = str_replace('fileadmin/', 'fileadmin/_processed_/', $src);

        $mimetype = '';
        if(function_exists('mime_content_type')){
            $mimetype = mime_content_type($src);
            //return $mimetype;

        } elseif(function_exists('finfo_open')){
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $src);
            finfo_close($finfo);
            //return $mimetype;
        }


        if(is_file($path) && $filter_radius != 0) {

            $bProcess = true;

            switch ($mimetype) {
                case 'image/jpeg':
                    $image=@imagecreatefromjpeg($path);
                    break;
                case 'image/png':
                    $image=@imagecreatefrompng($path);
                    break;
                default:
                    $bProcess = false;
            }

            $new_path= 'fileadmin/_processed_/' . str_replace('.', $suffixe . '.', basename($path));
            if(file_exists($new_path)) {
                $bProcess = false;
            }

            if ($bProcess) {

                switch ($mimetype) {
                    case 'image/png':
                        imagepng($image,$new_path, 0);
                        break;
                    default:
                        imagejpeg($image,$new_path, 0);
                }

                //
                //fastblur function from image hosting and processing site http://hero-in.com
                //
                $radius = $filter_radius;
                if ($radius>100) $radius=100; //max radius
                if ($radius<0) $radius=0; //nin radius

                $radius=$radius*4;
                $alphaStep=round(100/$radius)*1.7;
                $width=imagesx($image);
                $height=imagesy($image);
                $beginX=floor($radius/2);
                $beginY=floor($radius/2);


                //make clean imahe sample for multiply
                $cleanImageSample=imagecreatetruecolor($width, $height);
                imagecopy($cleanImageSample, $image, 0, 0, 0, 0, $width, $height);


                //make h blur
                for($i = 1; $i < $radius+1; $i++)
                {
                    $xPoint=($beginX*-1)+$i-1;
                    imagecopymerge($image, $cleanImageSample, $xPoint, 0, 0, 0, $width, $height, $alphaStep);
                }
                //make v blur
                imagecopy($cleanImageSample, $image, 0, 0, 0, 0, $width, $height);
                for($i = 1; $i < $radius+1; $i++)
                {
                    $yPoint=($beginY*-1)+$i-1;
                    imagecopymerge($image, $cleanImageSample, 0, $yPoint, 0, 0, $width, $height, $alphaStep);
                }

                switch ($mimetype) {
                    case 'image/png':
                        imagepng($image,$new_path, 0);
                        break;
                    default:
                        imagejpeg($image,$new_path, 0);
                }

                // Release memory
                imagedestroy($image);

            }
        }

        $this->tag->addAttribute('src', $new_path);
		$this->tag->addAttribute('width', $this->imageInfo[0]);
		$this->tag->addAttribute('height', $this->imageInfo[1]);
		if ('' === $this->arguments['title']) {
			$this->tag->addAttribute('title', $this->arguments['alt']);
		}

        return $new_path;
		//return $this->tag->render();
	}

}
