<?php
namespace HIVE\HiveViewhelpers\ViewHelpers;


/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner - hive GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class DateCombinerViewHelper extends AbstractViewHelper {


    /**
     * @param mixed $dStart
     * @param mixed $dEnd
     * @param mixed $sFormat
     * @return mixed
     */
    public function render($dStart, $dEnd, $sFormat) {

        $aCompareDates = [
            'start' => [
                'day' => intval(
                    $dStart->format('d')
                ),
                'month' => intval(
                    $dStart->format('m')
                ),
                'year' => intval(
                    $dStart->format('Y')
                )
            ],
            'end' => [
                'day' => intval(
                    $dEnd->format('d')
                ),
                'month' => intval(
                    $dEnd->format('m')
                ),
                'year' => intval(
                    $dEnd->format('Y')
                )
            ],
            'format' => $sFormat
            // %m/%d/%Y | %d.%m.%Y
        ];


        if($aCompareDates['start']['day'] && $aCompareDates['start']['month'] && $aCompareDates['start']['year'] && $aCompareDates['end']['day'] && $aCompareDates['end']['month'] && $aCompareDates['end']['year'] && $aCompareDates['format']){

            if($sFormat == '%d.%m.%Y'){
                // eu format

                if($aCompareDates['start']['year'] == $aCompareDates['end']['year']){

                    if($aCompareDates['start']['month'] == $aCompareDates['end']['month']){

                        if($aCompareDates['start']['day'] == $aCompareDates['end']['day']){

                            // all 3 components of both dates are equal
                            $sReturn = $this->formatDate(
                                $aCompareDates['format'],
                                $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                            );
                            return $sReturn;

                        } else {

                            // the 2 last components of both dates are equal but the day is different
                            if($aCompareDates['start']['day'] < $aCompareDates['end']['day']){

                                // start date string (cropped)
                                $sStartCropped = $this->formatDate(
                                    $aCompareDates['format'],
                                    $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                                );
                                $sStartCropped = substr(
                                    $sStartCropped,
                                    0,
                                    3
                                );

                                // end date string
                                $sEnd = $this->formatDate(
                                    $aCompareDates['format'],
                                    $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                                );

                                $sReturn = $sStartCropped .' - '. $sEnd;
                                return $sReturn;

                            } else {

                                // ERROR :: invalid range...start-date is bigger than end-date
                                return FALSE;

                            }

                        }

                    } else {

                        // the last component of both dates are equal but the day and month are different
                        if($aCompareDates['start']['day'] < $aCompareDates['end']['day']){

                            // start date string (cropped)
                            $sStartCropped = $this->formatDate(
                                $aCompareDates['format'],
                                $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                            );
                            $sStartCropped = substr(
                                $sStartCropped,
                                0,
                                6
                            );

                            // end date string
                            $sEnd = $this->formatDate(
                                $aCompareDates['format'],
                                $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                            );

                            $sReturn = $sStartCropped .' - '. $sEnd;
                            return $sReturn;

                        } else {

                            // ERROR :: invalid range...start-date is bigger than end-date
                            return FALSE;

                        }

                    }

                } else {

                    if($aCompareDates['start']['year'] < $aCompareDates['end']['year']){

                        $sStart = $this->formatDate(
                            $aCompareDates['format'],
                            $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                        );

                        // end date string
                        $sEnd = $this->formatDate(
                            $aCompareDates['format'],
                            $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                        );

                        $sReturn = $sStart .' - '. $sEnd;
                        return $sReturn;


                    } else {

                        // ERROR :: invalid range...start-date is bigger than end-date
                        return FALSE;

                    }

                }

            } else {

                // not eu format
                $sStart = $this->formatDate(
                    $aCompareDates['format'],
                    $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                );

                // end date string
                $sEnd = $this->formatDate(
                    $aCompareDates['format'],
                    $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                );

                $sReturn = $sStart .' - '. $sEnd;
                return $sReturn;

            }



        } else {

            // ERROR :: not all dates are set
            return FALSE;

        }

    }


    /**
     * @param $sFormat
     * @param $sDate
     * @return false|string
     */
    public function formatDate ($sFormat, $sDate){

        $sCleanFormat = str_replace('%', '', $sFormat);

        $sFormatedDate = date(
            $sCleanFormat,
            strtotime($sDate)
        );
        return $sFormatedDate;

    }

}